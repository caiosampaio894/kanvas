O projeto simula um curso onde existem 3 tipos de usuário. 
1 - Instrutor (is_staff == True and is_superuser == True).
2 - Facilitador (is_staff == True and is_superuser == False).
3 - Aluno (is_staff == False and is_superuser == False).

Cada tipo de usuário tem sua funçao e acesso à funcionalidades de acordo com as permissões criadas. Para iniciar o projeto é necessário criar o venv, instalar o django rest framework.

