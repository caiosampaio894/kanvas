from rest_framework.decorators import api_view

from activity.views import ActivityView, get_submission, submission
from django.urls import path

urlpatterns = [
    path('activities/', ActivityView.as_view()),
    path('activities/<int:activity_id>/', ActivityView.as_view()),
    path('activities/<int:activity_id>/submissions/', submission),
    path('submissions/', get_submission)
]