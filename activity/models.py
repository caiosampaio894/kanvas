from re import T
from django.contrib.auth.models import User
from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import IntegerField
from django.db.models.fields.related import ForeignKey, ManyToManyField


class Activity(models.Model):
    title = models.CharField(max_length=255)
    points = models.IntegerField()
    submissions = ManyToManyField('Submission')


class Submission(models.Model):
    grade = models.IntegerField()
    repo = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    acitvity = models.ForeignKey('Activity', on_delete=models.CASCADE)



