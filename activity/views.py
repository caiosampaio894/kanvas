from django.core.exceptions import ObjectDoesNotExist
from django.db.models.fields import NullBooleanField
from django.shortcuts import render
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.views import APIView
from activity.models import Activity, Submission
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, authenticate
from rest_framework.permissions import IsAuthenticated
from activity.permissions import ActivityPermission, Student
from activity.serializers import ActivitySerializer, SubmissionSerializer
import ipdb

class ActivityView(APIView):
    
    authentication_classes = [TokenAuthentication]
    permission_classes =  [ActivityPermission]

    def post(self, request):
        data = request.data
        serializer = ActivitySerializer(data=data)
        serializer.is_valid(raise_exception=True)
        if Activity.objects.filter(title=data['title']).exists():
            return Response({'error': 'Activity with this name already exists'}, status=status.HTTP_400_BAD_REQUEST)
        activity = Activity.objects.create(**data)
        serializer = ActivitySerializer(activity)
        
        

        return Response(serializer.data, status=status.HTTP_201_CREATED)
        

    def get(self, request, format=None):
        activity = Activity.objects.all()
        serializer = ActivitySerializer(activity, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, activity_id):
        data = request.data
        try:
            activity = Activity.objects.get(id=activity_id)
        except ObjectDoesNotExist:
            return Response({'msg': 'This activity does not exists'}, status=status.HTTP_400_BAD_REQUEST)
        
        serializer = ActivitySerializer(activity)

        if  activity.submissions.count() > 0:
            return Response({'error': 'You can not change an Activity with submissions'}, status=status.HTTP_400_BAD_REQUEST)


        activity.title = data['title']
        updated_activity = ActivitySerializer(activity)
        activity.save()

        return Response(updated_activity.data, status=status.HTTP_200_OK)




@api_view(['POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([Student])
def submission(request, activity_id):
    data = request.data

    try:
        Activity.objects.get(id=activity_id)
    except ObjectDoesNotExist:
        return Response({'msg': 'This activity does not exists'}, status=status.HTTP_400_BAD_REQUEST)

    serializer = SubmissionSerializer(data=data)
    serializer.is_valid(raise_exception=True)
    submission = Submission.objects.create(**data)
    serializer = SubmissionSerializer(submission)

    return Response(serializer.data, status=status.HTTP_201_CREATED)

@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def get_submission(request):
    submission = Submission.objects.all()
    serializer = SubmissionSerializer(submission)
    return Response(serializer.data, status=status.HTTP_200_OK)
    


