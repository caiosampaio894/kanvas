from rest_framework.permissions import BasePermission
from rest_framework import permissions

class ActivityPermission(BasePermission):

    def has_permission(self, request, view):
        if request.user.is_staff == True and request.user.is_superuser == True:
            return True

class Student(BasePermission):
    
    def has_permission(self, request, view):
        if request.user.is_staff == False and request.user.is_superuser == False:
            return True