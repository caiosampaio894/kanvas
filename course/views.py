from django.http.response import HttpResponseBadRequest, HttpResponse
from django.shortcuts import render
import ipdb
from rest_framework import serializers
from rest_framework.exceptions import NotFound
from django.core.exceptions import  ObjectDoesNotExist
from rest_framework.response import Response

from course.permissions import IsInsctructor
from .models import Course, User
from rest_framework.views import APIView
from rest_framework import status
from .serializers import CourseSerializer, UserSerializer
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication, authenticate
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from ipdb import set_trace

class UserView(APIView):
    
    def post(self, request):
        user_data = request.data

        # if User.objects.filter(username=user_data['username']):
        #     return Response({'msg': 'user already exists!'}, status=status.HTTP_409_CONFLICT)

        user = User.objects.create_user(**user_data)
        serializer = UserSerializer(user)

        

        return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def login(request):

    try:
        username = request.data['username']
        password = request.data['password']
    except KeyError:
        return HttpResponseBadRequest()


    user = authenticate(username=username, password=password)

    if user:
        token = Token.objects.get_or_create(user=user)[0]
        print(token)
        return Response({'token': token.key})

    return Response({'msg': 'Invalid Credentials'})


class CourseView(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes =  [IsInsctructor]
    
    def post(self, request):
        data = request.data

        if Course.objects.filter(name=data["name"]):
            return Response({'msg': 'Course with this name already exists'}, status=status.HTTP_400_BAD_REQUEST)

        
        course = Course.objects.create(**data)
        serializer = CourseSerializer(course)
        

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, course_id):
        
    
        data = request.data
        
        course_put = Course.objects.get(id=course_id)
        serializer = CourseSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        course_put.name = serializer.validated_data['name']
        course_put.save()
        

        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, course_id):
        try:
            course = Course.objects.get(id=course_id)
            course.delete()
            return HttpResponse(status=204)
            
        except ObjectDoesNotExist:
            return Response({'msg': 'Invalid course_id'}, status=status.HTTP_404_NOT_FOUND)

    def get(self, request, course_id=0):
        if course_id:
            try:
                course = Course.objects.get(id=course_id)
                serializer = CourseSerializer(course)
                return Response(serializer.data, status=status.HTTP_200_OK)

            except ObjectDoesNotExist:  
                return Response({"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND)
        
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    
    

@api_view(['PUT'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated, IsInsctructor])
def registration(request, course_id):
    data = request.data
    try:
        course_put = Course.objects.get(id=course_id)
    except ObjectDoesNotExist:
        return Response({"errors": "invalid course_id"}, status=status.HTTP_404_NOT_FOUND)
        
    try:
        for ids in data["user_ids"]:    
            registration = User.objects.get(id=ids)
            if registration.is_staff == False and registration.is_superuser == False:
                course_put.users.add(registration)
    except ObjectDoesNotExist:
        return Response({"errors": "invalid user_id list"}, status=status.HTTP_404_NOT_FOUND)
    
        # else:    
        #     return Response({"errors": "Only students can be enrolled in the course."}, status=status.HTTP_400_BAD_REQUEST)
    

    serializer = CourseSerializer(course_put)
    course_put.save() 

    return Response(serializer.data, status=status.HTTP_200_OK)
