from django.urls import path
from rest_framework.decorators import api_view
from .views import CourseView, UserView, login, registration

urlpatterns = [
    path('accounts/', UserView.as_view()),
    path('login/', login),
    path("courses/", CourseView.as_view()),
    path("courses/<int:course_id>/", CourseView.as_view()),
    path("courses/<int:course_id>/registrations/", registration)
]